#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/file.h>
#include<sys/stat.h>
#include<errno.h>
#include<string.h>
#define CONST 5

//возвращает число цифр в числе - 1 или 2. Предполагается, что число участников не может превышать 99

int number_characters(int n)
{

    if (n / 10 == 0)
        return 1;
    else return 2;
}
//запись в троку числа бойцов

void write_to_line(char *str3, char *str2, int num_fighter, int j)
{
    sprintf(str2, "%d", num_fighter);
    if (number_characters(num_fighter) == 1) {
        str3[j] = '0';
        str3[j + 1] = str2[0];
    } else {
        str3[j] = str2[0];
        str3[j + 1] = str2[1];
    }
    str3[j + 2] = '|';
}

int main()
{


    int fd, num_teams, rand_num, k, num_fighter, res, flag = 0;
    fd = open("file.txt", O_CREAT | O_RDWR | O_TRUNC);
    printf("Введите количество команд\n");
    scanf("%d", &num_teams);
    pid_t PidsProcess[num_teams];
    //строки для чтения, записи в файл и конвертации строки в число
    char str1[num_teams * 3 + 1], str2[2], str3[num_teams * 3 + 2];

    //структура для блокировки
    struct flock lock;
    memset(&lock, 0, sizeof (lock));
    lock.l_start = 0;
    lock.l_whence = SEEK_SET;
    lock.l_len = 0;

    srand(time(NULL));
    //формирование строки с начальным числом участников
    for (int i = 0; i < num_teams * 3; i = i + 3) {
        str1[i] = '1';
        str1[i + 1] = '0';
        str1[i + 2] = '|';
    }
    str1[num_teams * 3] = '\n';
    write(fd, str1, num_teams * 3 + 1);


    for (int i = 0; i < num_teams; i++) {
        PidsProcess[i] = fork();
        if (-1 == PidsProcess[i]) {
            perror("Ошибка!\n");
            exit(1);
        } else if (0 == PidsProcess[i]) {

            for (;;) {


                lock.l_type = F_WRLCK | F_RDLCK;
                res = fcntl(fd, F_SETLKW, &lock);
                printf("Блокирование файла процессом-командой №%d, результат опереации %d \n\n", i + 1, res);
                lseek(fd, -3 * num_teams - 1, SEEK_END);
                read(fd, str1, num_teams * 3);
                str1[num_teams * 3] = '\0';
                printf("Количество бойцов в командах %s \n\n", str1);
                //номер команды в строке
                k = 0;
                //парсер строки
                for (int j = 0; j < num_teams * 3; j = j + 3) {
                    str2[0] = str1[j];
                    str2[1] = str1[j + 1];
                    num_fighter = atoi(str2);
                    printf("У команды %d сейчас %d бойцов\n", k + 1, num_fighter);
                    if (num_fighter == 0)
                        exit(0);
                    //команда соперника
                    if (i != k) {
                        rand_num = 1 + rand() % num_fighter;
                        printf("Это команда соперника, было убито бойцов в количестве %d\n", rand_num);
                        num_fighter = num_fighter - rand_num;
                        if (num_fighter == 0) {
                            printf("Число бойцов в команде 0! Игра закончена! \n");
                            flag = 1;

                        }
                        write_to_line(str3, str2, num_fighter, j);
                        printf("В команде осталось: %s \n\n", str2);

                    }//команда процесса-потока
                    else {
                        rand_num = 1 + rand() % CONST;
                        printf("В эту команду будет добавлено %d бойцов\n", rand_num);
                        num_fighter = num_fighter + rand_num;
                        sprintf(str2, "%d", num_fighter);
                        write_to_line(str3, str2, num_fighter, j);
                        printf("В команде стало: %s \n\n", str2);
                    }
                    k++;
                }
                str3[3 * num_teams] = '\n';
                str3[3 * num_teams + 1] = '\0';
                lseek(fd, 0, SEEK_END);
                write(fd, str3, 3 * num_teams + 1);
                printf("У команд сейчас %s бойцов!\n", str3);
                lock.l_type = F_UNLCK;
                res = fcntl(fd, F_SETLKW, &lock);
                printf("Разблокирование файла процессом-командой №%d, результат операции %d \n\n", i + 1, res);
                if (flag == 1)
                    exit(0);

            }
        }

    }

    if (wait(0) != 0)
        for (int i = 0; i < num_teams; i++)
            kill(PidsProcess[i], SIGKILL);
    printf("Выполнение программы закончено!\n");
    close(fd);
    return 0;
}